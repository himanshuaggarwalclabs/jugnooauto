//
//  CustomerLoginViewController.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 09/03/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit
import MapKit

class CustomerLoginViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var menuItem : [String] = ["Coupons","Invite&Get FREE Rides","Rides","Fare Details","Help","Logout"]

    var mapView: MKMapView = MKMapView()
    var menuView  = UIView(frame: CGRectMake(-260, 0, screenWidth-120, screenHeight))
    var view2  = UIView(frame: CGRectMake(0, screenHeight/2+253, screenWidth, 80))
    var view3  = UIView(frame: CGRectMake(0, screenHeight/2-273.5, screenWidth, 60))
    var view4  = UIView(frame: CGRectMake(0, screenHeight/2+85, screenWidth, 160))
    let getAutobutton = UIButton()
    let menuOffbutton = UIButton()
    let gpsButton = UIButton()
    let assigningButton = UIButton()
    let cancelButton = UIButton()
    let calldriverButton = UIButton()
    var menuTableView: UITableView  =   UITableView()
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.menuView.alpha = 0
        self.menuTableView.tag = 3
        self.assigningButton.alpha = 0
        self.cancelButton.alpha = 0
        self.view3.alpha = 0
        self.view4.alpha = 0
        self.calldriverButton.alpha = 0
        self.creatingCustomerloginscreen()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func creatingCustomerloginscreen()
    {
        
        
        mapView.frame = CGRectMake(0, 0, screenWidth, screenHeight)
        mapView.mapType = MKMapType.Standard
        mapView.zoomEnabled = true
        mapView.scrollEnabled = true
        mapView.backgroundColor = UIColor.blueColor()
        self.view.addSubview(mapView)
        
        var view1  = UIView(frame: CGRectMake(0, screenHeight/2-333.5, screenWidth, 60))
        view1.backgroundColor = UIColor(red:68.0/255.0, green:73.0/255.0,blue:78.0/255.0,alpha:0.7)
        mapView.addSubview(view1)

        
        
        view2.backgroundColor = UIColor(red:250.0/255.0, green:249.0/255.0,blue:248.0/255.0,alpha:0.9)
        mapView.addSubview(view2)
        
        
        view3.backgroundColor = UIColor(red:250.0/255.0, green:249.0/255.0,blue:248.0/255.0,alpha:0.9)
        mapView.addSubview(view3)
        
        //---------------
        view4.backgroundColor = UIColor(red:250.0/255.0, green:249.0/255.0,blue:248.0/255.0,alpha:0.9)
        mapView.addSubview(view4)
        
        let driverImage = UIImage(named:"" )
        let driverImageView = UIImageView(image:driverImage)
        driverImageView.frame = CGRect(x:50,y:10, width:100, height:100)
        driverImageView.backgroundColor = UIColor.whiteColor()
        //driverImageView.layer.cornerRadius = 60
        //driverImageView.clipsToBounds = true
        view4.addSubview(driverImageView)

        
        let driverLabel = UILabel()
        // label1.text = "FAQs"
        // label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        // label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        // label1.textAlignment = .Center
        //label1.numberOfLines = 5
        driverLabel.backgroundColor = UIColor.grayColor()
        driverLabel.frame = CGRectMake(40, 120, 130, 25)
        view4.addSubview(driverLabel)

        
        let autoImage = UIImage(named:"" )
        let autoImageView = UIImageView(image:autoImage)
        autoImageView.frame = CGRect(x:220,y:10, width:100, height:100)
        autoImageView.backgroundColor = UIColor.whiteColor()
        //autoImageView.layer.cornerRadius = 60
        //autoImageView.clipsToBounds = true
        view4.addSubview(autoImageView)
        
        let autoLabel = UILabel()
        // label1.text = "FAQs"
        // label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        // label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        // label1.textAlignment = .Center
        //label1.numberOfLines = 5
        autoLabel.backgroundColor = UIColor.grayColor()
        autoLabel.frame = CGRectMake(210, 120, 130, 25)
        view4.addSubview(autoLabel)
        
        //---------------
        
        /*menuView.backgroundColor = UIColor(red:17.0/255.0, green:20.0/255.0,blue:20.0/255.0,alpha:0.9)
        mapView.addSubview(menuView)*/

        let profileImage = UIImage(named:"Defaultuser.jpg" )
        let profileImageView = UIImageView(image:profileImage)
        profileImageView.frame = CGRect(x:60,y:60, width:120, height:120)
        profileImageView.layer.cornerRadius = 60
        profileImageView.clipsToBounds = true
        menuView.addSubview(profileImageView)
        
        let profileLabel = UILabel()
        // label1.text = "FAQs"
        // label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        // label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        // label1.textAlignment = .Center
        //label1.numberOfLines = 5
        profileLabel.backgroundColor = UIColor.grayColor()
        profileLabel.frame = CGRectMake(35,205, 200, 30)
        menuView.addSubview(profileLabel)

        
        menuTableView.frame         =   CGRectMake(0,255, screenWidth-120, screenHeight-310);
        menuTableView.delegate      =   self
        menuTableView.dataSource    =   self
        menuTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        // menuTableView.backgroundColor = UIColor(red:17.0/255.0, green:20.0/255.0,blue:20.0/255.0,alpha:0.9)
        menuView.addSubview(menuTableView)
        
      
        let view2Label = UILabel()
       // label1.text = "FAQs"
       // label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
       // label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
       // label1.textAlignment = .Center
        //label1.numberOfLines = 5
        view2Label.backgroundColor = UIColor.grayColor()
        view2Label.frame = CGRectMake(screenWidth/2-92, 20, 200, 30)
        view2.addSubview(view2Label)
        
        let view3Label = UILabel()
        // label1.text = "FAQs"
        // label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        // label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        // label1.textAlignment = .Center
        //label1.numberOfLines = 5
        view3Label.backgroundColor = UIColor.grayColor()
        view3Label.frame = CGRectMake(screenWidth/2-92, 20, 200, 30)
        view3.addSubview(view3Label)

        let logoImage = UIImage(named:"jugnoo_logo.png" )
        let logoImageView = UIImageView(image:logoImage!)
        logoImageView.frame = CGRect(x: screenWidth/2-30,y:25, width: 75, height: 25)
        view1.addSubview(logoImageView)
        
        //let gpsImage = UIImage(named:"gps.png" )
        //let gpsImageView = UIImageView(image:gpsImage!)
        //gpsImageView.frame = CGRect(x: screenWidth/2-165,y:screenHeight/2+130, width: 41, height: 41)
        //mapView.addSubview(gpsImageView)
        
        
        //backButton.setTitle("Back", forState: .Normal)
        let image = UIImage(named: "menu_off.png") as UIImage?
        menuOffbutton.setImage(image, forState: .Normal)
        menuOffbutton.frame = CGRectMake(10,15, 44, 44)
        menuOffbutton.addTarget(self, action: "menuPressed", forControlEvents: .TouchUpInside)
        view1.addSubview(menuOffbutton)
        
        
        //backButton.setTitle("Back", forState: .Normal)
        let gpsImage = UIImage(named: "gps.png") as UIImage?
        gpsButton.setImage(gpsImage, forState: .Normal)
        gpsButton.frame = CGRect(x: screenWidth/2-165,y:screenHeight/2+130, width: 41, height: 41)
        gpsButton.addTarget(self, action: "gpsPressed", forControlEvents: .TouchUpInside)
        mapView.addSubview(gpsButton)
        
        
        getAutobutton.setTitle("Get an Auto", forState: .Normal)
        getAutobutton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        getAutobutton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        getAutobutton.layer.cornerRadius = 5
        getAutobutton.frame = CGRectMake(screenWidth/2-165,screenHeight/2+180, 330, 60)
        getAutobutton.addTarget(self, action: "getAutopressed", forControlEvents: .TouchUpInside)
        mapView.addSubview(getAutobutton)

        assigningButton.setTitle("Assigning driver..", forState: .Normal)
        assigningButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        assigningButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        assigningButton.layer.cornerRadius = 5
        assigningButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2+180, 330, 50)
        assigningButton.addTarget(self, action: "assigningPressed", forControlEvents: .TouchUpInside)
        mapView.addSubview(assigningButton)

        cancelButton.setTitle("Cancel Request", forState: .Normal)
        cancelButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        cancelButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        cancelButton.layer.cornerRadius = 5
        cancelButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2+240, 330, 50)
        cancelButton.addTarget(self, action: "cancelPressed", forControlEvents: .TouchUpInside)
        mapView.addSubview(cancelButton)
        
        calldriverButton.setTitle("Call Driver", forState: .Normal)
        calldriverButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        calldriverButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        calldriverButton.layer.cornerRadius = 5
        calldriverButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2+260, 330, 50)
        calldriverButton.addTarget(self, action: "calldriverPressed", forControlEvents: .TouchUpInside)
        mapView.addSubview(calldriverButton)
        
        menuView.backgroundColor = UIColor(red:17.0/255.0, green:20.0/255.0,blue:20.0/255.0,alpha:0.9)
        mapView.addSubview(menuView)


    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItem.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
        cell.textLabel?.text = menuItem[indexPath.row]
        cell.textLabel?.textColor = UIColor.whiteColor()
        cell.backgroundColor = UIColor(red:17.0/255.0, green:20.0/255.0,blue:20.0/255.0,alpha:0.9)
        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let height: CGFloat = 60
        return height
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("You selected cell #\(indexPath.row)!")
        if indexPath.row == 0 {
            let couponView = self.storyboard?.instantiateViewControllerWithIdentifier("couponView") as CouponsViewController
            self.navigationController?.pushViewController(couponView, animated: true)

        }else if indexPath.row == 1{
            
             let shareView = self.storyboard?.instantiateViewControllerWithIdentifier("shareView") as ShareViewController
            self.navigationController?.pushViewController(shareView, animated: true)

        }else if indexPath.row == 2 {
            let rideView = self.storyboard?.instantiateViewControllerWithIdentifier("rideView") as RidesViewController
            self.navigationController?.pushViewController(rideView, animated: true)

        }else if indexPath.row == 3 {
            let fareView = self.storyboard?.instantiateViewControllerWithIdentifier("fareView") as FareDeatailsViewController
            self.navigationController?.pushViewController(fareView, animated: true)
        }else if indexPath.row == 4 {
            let helpView = self.storyboard?.instantiateViewControllerWithIdentifier("helpView") as HelpViewController
            self.navigationController?.pushViewController(helpView, animated: true)
        }else if indexPath.row == 5 {
            
            var logalert = UIAlertController(title: "Logout", message: "Are you sure you want to logout", preferredStyle: UIAlertControllerStyle.Alert)
            logalert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default){ (action) in
                //self.playAgian()
                })
            // AlertView Action used to show the Players ScoreBoard
            logalert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default){ (action) in
                
                let firstView = self.storyboard?.instantiateViewControllerWithIdentifier("firstView") as SplashViewController
                self.navigationController?.pushViewController(firstView, animated: true)
                })
            self.presentViewController(logalert, animated: true, completion: nil)        }
    }
    

    
    func menuPressed(){
       // getAutobutton.alpha = 0
        //gpsButton.alpha = 0
        //assigningButton.alpha = 0
       // cancelButton.alpha = 0
       // calldriverButton.alpha = 0
        //view4.alpha = 0
        //menuView.alpha = 1
        UIView.animateWithDuration(0.2, animations: {
            self.menuView.center = CGPointMake(self.menuView.center.x+260, self.menuView.center.y)
        })
    }

    func getAutopressed(){
        
        var alert1 = UIAlertController(title: "Chalo Jugnoo Se", message: "Do you want an auto to pick you up ", preferredStyle: UIAlertControllerStyle.Alert)
        alert1.addAction(UIAlertAction(title: "Later", style: UIAlertActionStyle.Default){ (action) in
            //self.playAgian()
            })
        // AlertView Action used to show the Players ScoreBoard
        alert1.addAction(UIAlertAction(title: "Get Now", style: UIAlertActionStyle.Default){ (action) in
            //self.scoreBoard()
            self.view2.alpha = 0
            self.view3.alpha = 1
            self.getAutobutton.alpha = 0
            self.assigningButton.alpha = 1
            self.cancelButton.alpha = 1
            })
        self.presentViewController(alert1, animated: true, completion: nil)

    }
    
    func gpsPressed(){
        println("GPSPressed")
    }
    
    func cancelPressed(){
        getAutobutton.alpha = 1
        view2.alpha = 1
        assigningButton.alpha = 0
        cancelButton.alpha = 0
        view3.alpha = 0
    }
    func assigningPressed(){
        
        assigningButton.alpha = 0
        cancelButton.alpha = 0
        calldriverButton.alpha = 1
        gpsButton.frame = CGRect(x: screenWidth/2-165,y:screenHeight/2+20, width: 41, height: 41)
        view4.alpha = 1
        
    }
    func calldriverPressed(){
        
        let rideOverview = self.storyboard?.instantiateViewControllerWithIdentifier("rideOverview") as RideOverViewController
        self.navigationController?.pushViewController(rideOverview, animated: true)
        println("CALLING")
        }
    
    
    override  func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        UIView.animateWithDuration(0.2, animations: {
            self.menuView.center = CGPointMake(self.menuView.center.x-260, self.menuView.center.y)
        })
       // getAutobutton.alpha = 1
       // gpsButton.alpha = 1
        
    }
        
  }
