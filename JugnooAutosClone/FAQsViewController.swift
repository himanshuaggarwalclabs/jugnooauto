//
//  FAQsViewController.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 03/03/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit

class FAQsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.creatingFAQscreen()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func creatingFAQscreen(){
        
        var view1  = UIView(frame: CGRectMake(0, screenHeight/2-333.5, screenWidth, 80))
        view1.backgroundColor = UIColor(red:36.0/255.0, green:41.0/255.0,blue:48.0/255.0,alpha:1.0)
        self.view.addSubview(view1)
        
        
        let label1 = UILabel()
        label1.text = "FAQs"
        label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        label1.textAlignment = .Center
        //label1.numberOfLines = 5
        label1.frame = CGRectMake(screenWidth/2-62, 40, 150, 30)
        view1.addSubview(label1)
        
        /*let label2 = UILabel()
        label2.text = "Want more FREE rides?"
        label2.font = UIFont(name: "MarkerFelt-Thin", size: 20)
        label2.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        label2.backgroundColor = UIColor(red:32.0/255.0, green:32.0/255.0,blue:32.0/255.0,alpha:1.0)
        //label2.textAlignment = .Center
        //label1.numberOfLines = 5
        label2.frame = CGRectMake(0, screenHeight/2-253.5, 375, 60)
        self.view.addSubview(label2)*/
        
        let backButton = UIButton()
        //backButton.setTitle("Back", forState: .Normal)
        let image = UIImage(named: "back-button.png") as UIImage?
        backButton.setImage(image, forState: .Normal)
        //backButton.backgroundImageForState("back-button.png")
        //backButton.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        //backButton.backgroundColor = UIColor.cyanColor()
        //backButton.layer.cornerRadius = 5
        backButton.frame = CGRectMake(screenWidth/2-190,30, 50, 50)
        backButton.addTarget(self, action: "BackPressed", forControlEvents: .TouchUpInside)
        view1.addSubview(backButton)

    }
    
    func BackPressed(){
        navigationController!.popToViewController(navigationController!.viewControllers[2] as UIViewController, animated: true)
    }
}
