//
//  ViewController.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 24/02/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit

var JUGNOO_KEYCHAIN_SERVICE = "jugnoo_keychain_service"
var JUGNOO_AUTH_KEY = "jugnoo_auth_key"

let clientName = "JugnooAutos"
let clientIdentifier = "EEBUOvQq7RRJBxJm"
let clientSharedSecret = "nqaK7HTwDT3epcpR5JuMWwojFv0KJnIv"
var accessToken = ""

let screenSize: CGRect = UIScreen.mainScreen().bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height
//------------
let splashBgimage = "jugnoo_splash.jpg"
let bgImage = UIImage(named: splashBgimage)
let splashBgimageView = UIImageView(image: bgImage!)

class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.creatingSplashscreen()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func creatingSplashscreen(){
        
        let splashBgimage = "jugnoo_splash.jpg"
        let bgImage = UIImage(named: splashBgimage)
        let splashBgimageView = UIImageView(image: bgImage!)
        splashBgimageView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        view.addSubview(splashBgimageView)
        
        let splashIconimage = "splash_img_2.png"
        let iconImage = UIImage(named: splashIconimage)
        let splashIconimageView = UIImageView(image: iconImage!)
        splashIconimageView.frame = CGRect(x: screenWidth/2-80,y: screenHeight/2-230, width: 160, height: 166)
        view.addSubview(splashIconimageView)
        
        let splashJgimage = "jugnoo_text.png"
        let jgImage = UIImage(named: splashJgimage)
        let splashJgimageView = UIImageView(image: jgImage!)
        splashJgimageView.frame = CGRect(x: screenWidth/2-150,y: screenHeight/2-40, width: 290, height: 90)
        view.addSubview(splashJgimageView)
         //--------------
        let registerButton = UIButton()
        registerButton.setTitle("REGISTER", forState: .Normal)
        registerButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        registerButton.backgroundColor = UIColor.cyanColor()
        registerButton.layer.cornerRadius = 5
        registerButton.frame = CGRectMake(screenWidth/2+10,screenHeight/2+170, 160, 60)
        registerButton.addTarget(self, action: "registerPressed:", forControlEvents: .TouchUpInside)
        self.view.addSubview(registerButton)
        
        let loginButton = UIButton()
        loginButton.setTitle("LOGIN", forState: .Normal)
        loginButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        loginButton.backgroundColor = UIColor.cyanColor()
        loginButton.layer.cornerRadius = 5
        loginButton.frame = CGRectMake(screenWidth/2-170,screenHeight/2+170, 160, 60)
        loginButton.addTarget(self, action: "loginPressed:", forControlEvents: .TouchUpInside)
        self.view.addSubview(loginButton)
        //--------------
        if screenHeight == 480 {
            //splashBgimageView.frame = CGRect(x: 0, y: 0, width: 320, height: 480)
            splashIconimageView.frame = CGRect(x: screenWidth/2-50,y: screenHeight/2-155, width: 105, height: 111)
            splashJgimageView.frame = CGRect(x: screenWidth/2-120,y: screenHeight/2-20, width: 245, height: 45)
            registerButton.frame = CGRectMake(screenWidth/2+10,screenHeight/2+140, 120, 50)
            loginButton.frame = CGRectMake(screenWidth/2-130,screenHeight/2+140, 120, 50)
        }else if screenHeight == 568 {
            splashBgimageView.frame = CGRect(x: 0, y: 0, width: 320, height: 568)
            splashIconimageView.frame = CGRect(x: screenWidth/2-50,y: screenHeight/2-195, width: 105, height: 135)
            splashJgimageView.frame = CGRect(x: screenWidth/2-120,y: screenHeight/2-40, width: 245, height: 45+58)
            registerButton.frame = CGRectMake(screenWidth/2+10,screenHeight/2+140, 120, 50)
            loginButton.frame = CGRectMake(screenWidth/2-130,screenHeight/2+140, 120, 50)
        }else if screenHeight == 736 {
            splashBgimageView.frame = CGRect(x: 0, y: 0, width: 414, height: 736)
            splashIconimageView.frame = CGRect(x: screenWidth/2-70,y: screenHeight/2-230, width: 130, height: 131)
            splashJgimageView.frame = CGRect(x: screenWidth/2-140,y: screenHeight/2-60, width: 275, height: 75)
            registerButton.frame = CGRectMake(screenWidth/2+30,screenHeight/2+160, 150, 80)
            loginButton.frame = CGRectMake(screenWidth/2-170,screenHeight/2+160, 150, 80)
        }
    }
    //--------------
    func registerPressed(sender: UIButton!){
        
        let registerView = self.storyboard?.instantiateViewControllerWithIdentifier("registerView") as RegisterViewController
        self.navigationController?.pushViewController(registerView, animated: true)
        //let rideOverview = self.storyboard?.instantiateViewControllerWithIdentifier("rideOverview") as RideOverViewController
        //self.navigationController?.pushViewController(rideOverview, animated: true)
    }
    
    func loginPressed(sender: UIButton!){
        
        let loginView = self.storyboard?.instantiateViewControllerWithIdentifier("loginView") as LoginViewController
        self.navigationController?.pushViewController(loginView, animated: true)
    }
    
}

