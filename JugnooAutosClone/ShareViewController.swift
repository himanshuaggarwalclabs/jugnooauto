//
//  ShareViewController.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 26/02/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController {

    var myString:NSString = "Your Code"
    var myMutableString = NSMutableAttributedString()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.creatingSharescreen()
        // Do any additional setup after loading the view.
        /*myMutableString = NSMutableAttributedString(string: myString, attributes: [NSFontAttributeName:UIFont(name: "MarkerFelt-Thin", size: 18.0)!])
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: NSRange(location:1,length:10))
        //testLabel.attributedText = myMutableString
         println(myMutableString)
        //println(testLabel.text!)*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func creatingSharescreen(){
        
      
        
        splashBgimageView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        view.addSubview(splashBgimageView)
        
        
        //let splashIconimage = "splash_img_2.png"
        let iconImage = UIImage(named:"splash_img_2.png" )
        let iconImageView = UIImageView(image: iconImage!)
        iconImageView.frame = CGRect(x: screenWidth/2-70,y: screenHeight/2-200, width: 120, height: 130)
        view.addSubview(iconImageView)
        
        
        //let splashJgimage = "jugnoo_text.png"
        let jgImage = UIImage(named: "jugnoo_text.png")
        let jgImageView = UIImageView(image: jgImage!)
        jgImageView.frame = CGRect(x: screenWidth/2-120,y: screenHeight/2-45, width: 220, height: 70)
        view.addSubview(jgImageView)
        
        var view1  = UIView(frame: CGRectMake(0, screenHeight/2-333.5, 375, 80))
        view1.backgroundColor = UIColor(red:36.0/255.0, green:41.0/255.0,blue:48.0/255.0,alpha:1.0)
        self.view.addSubview(view1)
        
        let label1 = UILabel()
        label1.text = "Share"
        label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        label1.textAlignment = .Center
        //label1.numberOfLines = 5
        label1.frame = CGRectMake(screenWidth/2-62, screenHeight/2-300, 100, 30)
        self.view.addSubview(label1)
     

    
        let label3 = UILabel()
        label3.text = "Your code"
        //label3.attributedText = myMutableString
       // println(myMutableString)
        label3.font = UIFont(name: "MarkerFelt-Thin", size: 20)
        label3.textColor = UIColor(red:253.0/255.0, green:186.0/255.0,blue:20.0/255.0,alpha:1.0)
        //label2.textAlignment = .Center
        label3.numberOfLines = 1
        label3.frame = CGRectMake(screenWidth/2+40, screenHeight/2+40, 100, 20)
        self.view.addSubview(label3)
        
        let label2 = UILabel()
        label2.text = "Your Referral Code is \(label3.text!) "
        label2.font = UIFont(name: "MarkerFelt-Thin", size: 20)
        label2.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        //label2.textAlignment = .Center
        label2.numberOfLines = 1
        label2.frame = CGRectMake(screenWidth/2-140, screenHeight/2+40, 184, 20)
        self.view.addSubview(label2)
        
        let label4 = UILabel()
        
        label4.text = "Share your referral code \(label3.text!) with \n your friends and they will get a FREE ride\n because of your referral and once they have\n   used jugnoo,you will earn a FREE ride\n            (upto RS.100) as well. "
        label4.font = UIFont(name: "MarkerFelt-Thin", size: 20)
        label4.textColor = UIColor(red:129.0/255.0, green:129.0/255.0,blue:129.0/255.0,alpha:1.0)
        //label4.adjustsFontSizeToFitWidth = true
        //label2.textAlignment = .Center
        label4.backgroundColor = UIColor.whiteColor()
        label4.numberOfLines = 6
        label4.frame = CGRectMake(screenWidth/2-187.5, screenHeight/2+134, 375, 200.5)
        self.view.addSubview(label4)


        let backButton = UIButton()
        //backButton.setTitle("Back", forState: .Normal)
        let image = UIImage(named: "back-button.png") as UIImage?
        backButton.setImage(image, forState: .Normal)
        //backButton.backgroundImageForState("back-button.png")
        //backButton.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        //backButton.backgroundColor = UIColor.cyanColor()
        //backButton.layer.cornerRadius = 5
        backButton.frame = CGRectMake(screenWidth/2-190,screenHeight/2-310, 50, 50)
        backButton.addTarget(self, action: "BackPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(backButton)
        
        let socialshareButton1 = UIButton()
        let ssimage1 = UIImage(named: "social_share_button_1.png") as UIImage?
        socialshareButton1.setImage(ssimage1, forState: .Normal)
        socialshareButton1.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        socialshareButton1.frame = CGRectMake(screenWidth/2-150,screenHeight/2+70, 45, 45)
        socialshareButton1.addTarget(self, action: "ss1Pressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(socialshareButton1)

        let socialshareButton2 = UIButton()
        let ssimage2 = UIImage(named: "social_share_button_2.png") as UIImage?
        socialshareButton2.setImage(ssimage2, forState: .Normal)
        socialshareButton2.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        socialshareButton2.frame = CGRectMake(screenWidth/2-70,screenHeight/2+70, 45, 45)
        socialshareButton2.addTarget(self, action: "ss2Pressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(socialshareButton2)
        
        let socialshareButton3 = UIButton()
        let ssimage3 = UIImage(named: "social_share_button_3.png") as UIImage?
        socialshareButton3.setImage(ssimage3, forState: .Normal)
        socialshareButton3.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        socialshareButton3.frame = CGRectMake(screenWidth/2+10,screenHeight/2+70, 45, 45)
        socialshareButton3.addTarget(self, action: "ss3Pressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(socialshareButton3)
        
        let socialshareButton4 = UIButton()
        let ssimage4 = UIImage(named: "social_share_button_4.png") as UIImage?
        socialshareButton4.setImage(ssimage4, forState: .Normal)
        socialshareButton4.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        socialshareButton4.frame = CGRectMake(screenWidth/2+90,screenHeight/2+70, 45, 45)
        socialshareButton4.addTarget(self, action: "ss4Pressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(socialshareButton4)

    }
    func BackPressed(){
        navigationController!.popToViewController(navigationController!.viewControllers[2] as UIViewController, animated: true)
    }
}
