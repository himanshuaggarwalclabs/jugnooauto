//
//  FareDeatailsViewController.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 03/03/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit

class FareDeatailsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.creatingFaredetailsscreen()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

func creatingFaredetailsscreen(){
    
    var view1  = UIView(frame: CGRectMake(0, screenHeight/2-333.5, screenWidth, 80))
    view1.backgroundColor = UIColor(red:36.0/255.0, green:41.0/255.0,blue:48.0/255.0,alpha:1.0)
    self.view.addSubview(view1)
    
    
    let label1 = UILabel()
    label1.text = "Fare Deatails"
    label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
    label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
    label1.textAlignment = .Center
    //label1.numberOfLines = 5
    label1.frame = CGRectMake(screenWidth/2-62, screenHeight/2-300, 150, 30)
    self.view.addSubview(label1)

    
    let label2 = UILabel()
    label2.text = "The fare for first 2 kms is Rs.30 after which Rs \n .10 per km will be charged"
    label2.font = UIFont(name: "MarkerFelt-Thin", size: 18)
    label2.textColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:1.0)
    //label2.backgroundColor = UIColor(red:32.0/255.0, green:32.0/255.0,blue:32.0/255.0,alpha:1.0)
    //label2.textAlignment = .Center
    label2.frame = CGRectMake(5, screenHeight/2-243.5, screenWidth, 40)
    label2.numberOfLines = 4
    self.view.addSubview(label2)


    let backButton = UIButton()
    //backButton.setTitle("Back", forState: .Normal)
    let image = UIImage(named: "back-button.png") as UIImage?
    backButton.setImage(image, forState: .Normal)
    //backButton.backgroundImageForState("back-button.png")
    //backButton.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
    //backButton.backgroundColor = UIColor.cyanColor()
    //backButton.layer.cornerRadius = 5
    backButton.frame = CGRectMake(screenWidth/2-190,screenHeight/2-310, 50, 50)
    backButton.addTarget(self, action: "BackPressed", forControlEvents: .TouchUpInside)
    self.view.addSubview(backButton)
    
}
    func BackPressed(){
        navigationController!.popToViewController(navigationController!.viewControllers[2] as UIViewController, animated: true)
    }

}