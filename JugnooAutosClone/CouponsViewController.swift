//
//  CouponsViewController.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 02/03/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit

class CouponsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.creatingCouponScreen()
    
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func creatingCouponScreen(){
        
        var view1  = UIView(frame: CGRectMake(0, screenHeight/2-333.5, screenWidth, 80))
        view1.backgroundColor = UIColor(red:36.0/255.0, green:41.0/255.0,blue:48.0/255.0,alpha:1.0)
        self.view.addSubview(view1)
        
        /*let couponImage = UIImage(named:"coupon_account.png" )
        let couponImageView = UIImageView(image: couponImage!)
        couponImageView.frame = CGRect(x: 30,y: screenHeight/2-80, width: 302, height: 114)
        view.addSubview(couponImageView)*/
        
        let label1 = UILabel()
        label1.text = "Coupons"
        label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        label1.textAlignment = .Center
        //label1.numberOfLines = 5
        label1.frame = CGRectMake(screenWidth/2-62, screenHeight/2-300, 100, 30)
        self.view.addSubview(label1)
        
        let label2 = UILabel()
        label2.text = "Want more FREE rides?"
        label2.font = UIFont(name: "MarkerFelt-Thin", size: 20)
        label2.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        label2.backgroundColor = UIColor(red:32.0/255.0, green:32.0/255.0,blue:32.0/255.0,alpha:1.0)
        //label2.textAlignment = .Center
        //label1.numberOfLines = 5
        label2.frame = CGRectMake(0, screenHeight/2-253.5, 375, 60)
        self.view.addSubview(label2)

        let label3 = UILabel()
        label3.text = "Enter referral/promo code to get FREE rides:"
        label3.font = UIFont(name: "MarkerFelt-Thin", size: 15)
        label3.textColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:1.0)
        //label3.textColor = UIColor.blackColor()
        //label3.textAlignment = .Center
        //label1.numberOfLines = 5
        label3.frame = CGRectMake(5, screenHeight/2-190, 300, 30)
        self.view.addSubview(label3)
        
        /*let label4 = UILabel()
        label4.text = "  You Have \n0 Free Rides"
        label4.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label4.textColor = UIColor(red:143.0/255.0, green:103.0/255.0,blue:29.0/255.0,alpha:1.0)
        //label3.textColor = UIColor.blackColor()
        //label3.textAlignment = .Center
        label4.numberOfLines = 5
        label4.frame = CGRectMake(120, screenHeight/2-60, 200, 60)
        self.view.addSubview(label4)*/


        var referralField = UITextField(frame:CGRectMake(5, screenHeight/2-155, 350, 45))
        referralField.backgroundColor = UIColor.whiteColor()
        referralField.borderStyle = UITextBorderStyle.Line
        //emailField = UITextField (frame:CGRectMake(screenWidth/2, screenHeight/2, 200, 30));
        // emailField.layer.cornerRadius = 5
        referralField.placeholder = "Enter Referral/Promo code"
        referralField.borderStyle = .RoundedRect
        self.view.addSubview(referralField)

        let backButton = UIButton()
        //backButton.setTitle("Back", forState: .Normal)
        let image = UIImage(named: "back-button.png") as UIImage?
        backButton.setImage(image, forState: .Normal)
        //backButton.backgroundImageForState("back-button.png")
        //backButton.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        //backButton.backgroundColor = UIColor.cyanColor()
        //backButton.layer.cornerRadius = 5
        backButton.frame = CGRectMake(screenWidth/2-190,screenHeight/2-310, 50, 50)
        backButton.addTarget(self, action: "BackPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(backButton)
        
        let referButton = UIButton()
        referButton.setTitle("Refer us", forState: .Normal)
        referButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        referButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        referButton.layer.cornerRadius = 5
        referButton.frame = CGRectMake(screenWidth/2+55,screenHeight/2-242, 120, 35)
        referButton.addTarget(self, action: "referPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(referButton)
        
        
        var couponTableView: UITableView  =   UITableView()
        couponTableView.frame         =   CGRectMake(0,screenHeight-430, screenWidth, screenHeight+50);
        couponTableView.delegate      =   self
        couponTableView.dataSource    =   self
        
        couponTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        //couponTableView.backgroundColor = UIColor.cyanColor()
        self.view.addSubview(couponTableView)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
        
       // cell.textLabel?.text = "hidfbjkfgj"
        var cellView = UIView(frame: CGRectMake(2, 2, screenWidth, 120))
        //cellView.backgroundColor = UIColor.redColor()
       cell.contentView.addSubview(cellView)
       // var couponImage = UIImage(named:"coupon_account.png" )
        //var cellImage = UIImageView(image: couponImage!)
        var cellImage = UIImageView(frame: CGRectMake(30, 5, 302, 114))
        cellImage.image = UIImage(named: "coupon_account.png")
        //cellImage.backgroundColor = UIColor.greenColor()
        cellView.addSubview(cellImage)
        
        let cellLabel = UILabel()
        cellLabel.text = "  You Have \n0 Free Rides"
        cellLabel.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        cellLabel.textColor = UIColor(red:143.0/255.0, green:103.0/255.0,blue:29.0/255.0,alpha:1.0)
        //label3.textColor = UIColor.blackColor()
        //label3.textAlignment = .Center
        cellLabel.numberOfLines = 5
        cellLabel.frame = CGRectMake(80, 30, 200, 60)
        cellImage.addSubview(cellLabel)
        return cell
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let height: CGFloat = 120
        return height
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("You selected cell #\(indexPath.row)!")
    }
    func BackPressed(){
        navigationController!.popToViewController(navigationController!.viewControllers[2] as UIViewController, animated: true)
    }
    func referPressed(){
        println("Referpress")
    }
}
