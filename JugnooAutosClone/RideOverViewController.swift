//
//  RideOverViewController.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 12/03/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit

class RideOverViewController: UIViewController {

    //var menuView  = UIView(frame: CGRectMake(-260, 0, screenWidth-120, screenHeight))
    override func viewDidLoad() {
        super.viewDidLoad()

        self.creatingrideoverscreen()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func creatingrideoverscreen(){
        
        let coverImage = UIImage(named:"myimage.jpg" )
        let coverImageView = UIImageView(image:coverImage)
        coverImageView.frame = CGRect(x:0,y:0, width: screenWidth, height:screenHeight/2)
       // coverImageView.layer.cornerRadius = 60
        //coverImageView.clipsToBounds = true
        view.addSubview(coverImageView)
        
        let menuOffbutton = UIButton()
        let image = UIImage(named: "menu_off.png") as UIImage?
        menuOffbutton.setImage(image, forState: .Normal)
        menuOffbutton.frame = CGRectMake(10,15, 44, 44)
        menuOffbutton.addTarget(self, action: "menuPressed", forControlEvents: .TouchUpInside)
        coverImageView.addSubview(menuOffbutton)
        
        let logoImage = UIImage(named:"jugnoo_logo.png" )
        let logoImageView = UIImageView(image:logoImage!)
        logoImageView.frame = CGRect(x: screenWidth/2-30,y:25, width: 75, height: 25)
        coverImageView.addSubview(logoImageView)

        let profileImage = UIImage(named:"Defaultuser.jpg" )
        let profileImageView = UIImageView(image:profileImage)
        profileImageView.frame = CGRect(x:130,y:80, width:120, height:120)
        profileImageView.layer.cornerRadius = 60
        profileImageView.layer.borderWidth = 10
        profileImageView.layer.borderColor = UIColor(red:236.0/255.0, green:237.0/255.0,blue:234.0/255.0,alpha:0.7).CGColor
        profileImageView.clipsToBounds = true
        coverImageView.addSubview(profileImageView)
        
        let profileLabel = UILabel()
        // label1.text = "FAQs"
        // label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        // label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        // label1.textAlignment = .Center
        //label1.numberOfLines = 5
        profileLabel.backgroundColor = UIColor.grayColor()
        profileLabel.frame = CGRectMake(90,205, 200, 30)
        coverImageView.addSubview(profileLabel)
        
        var view1  = UIView(frame: CGRectMake(0,screenHeight/2-95 , screenWidth, 95))
        view1.backgroundColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:0.5)
        coverImageView.addSubview(view1)
       
        
        let view1Label = UILabel()
        view1Label.text = "Reached Destination"
        view1Label.font = UIFont(name: "MarkerFelt-Thin", size: 20)
        view1Label.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        view1Label.textAlignment = .Center
        //label1.numberOfLines = 5
       // view1Label.backgroundColor = UIColor.grayColor()
        view1Label.frame = CGRectMake(90, 5, 200, 30)
        view1.addSubview(view1Label)
        
        let distanceLabel = UILabel()
        distanceLabel.text = "Distance\n  1.1 kms"
        distanceLabel.font = UIFont(name: "MarkerFelt-Thin", size: 20)
        distanceLabel.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        distanceLabel.textAlignment = .Center
        distanceLabel.numberOfLines = 5
        // view1Label.backgroundColor = UIColor.grayColor()
        distanceLabel.frame = CGRectMake(20,35,100, 50)
        view1.addSubview(distanceLabel)
        
        var view2  = UIView(frame: CGRectMake(130, 35, 1, 50))
        view2.backgroundColor = UIColor.whiteColor()
        view1.addSubview(view2)
        
        let rideTimelabel = UILabel()
        rideTimelabel.text = "Ride Time\n 1min"
        rideTimelabel.font = UIFont(name: "MarkerFelt-Thin", size: 20)
        rideTimelabel.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        rideTimelabel.textAlignment = .Center
        rideTimelabel.numberOfLines = 5
        // view1Label.backgroundColor = UIColor.grayColor()
        rideTimelabel.frame = CGRectMake(140,35,100, 50)
        view1.addSubview(rideTimelabel)
        
        var view3  = UIView(frame: CGRectMake(250, 35, 1, 50))
        view3.backgroundColor = UIColor.whiteColor()
        view1.addSubview(view3)

        let farelabel = UILabel()
        farelabel.text = "Fare\n Rs. 25"
        farelabel.font = UIFont(name: "MarkerFelt-Thin", size: 20)
        farelabel.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        farelabel.textAlignment = .Center
        farelabel.numberOfLines = 5
        // view1Label.backgroundColor = UIColor.grayColor()
        farelabel.frame = CGRectMake(260,35,100, 50)
        view1.addSubview(farelabel)
     
        //--------------
        
       let label1 = UILabel()
        label1.text = "The Jugnoo ride is over."
        label1.font = UIFont(name: "MarkerFelt-Thin", size: 20)
        label1.textColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:1.0)
        label1.textAlignment = .Center
        label1.numberOfLines = 5
        // view1Label.backgroundColor = UIColor.grayColor()
        label1.frame = CGRectMake(screenWidth/2-100,screenHeight/2+40,200, 30)
        view.addSubview(label1)
        
        let label2 = UILabel()
        label2.text = "Please pay the fare as shown above\n         to the driver."
        label2.font = UIFont(name: "MarkerFelt-Thin", size: 20)
        label2.textColor = UIColor(red:130.0/255.0, green:130.0/255.0,blue:130.0/255.0,alpha:1.0)
        //label2.textAlignment = .Center
        label2.numberOfLines = 5
        // view1Label.backgroundColor = UIColor.grayColor()
        label2.frame = CGRectMake(screenWidth/2-150,screenHeight/2+70,300, 50)
        view.addSubview(label2)
        
        let okButton = UIButton()
        okButton.setTitle("OK", forState: .Normal)
        okButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        okButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        okButton.layer.cornerRadius = 5
        okButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2+150, 330, 50)
        okButton.addTarget(self, action: "okPressed", forControlEvents: .TouchUpInside)
        view.addSubview(okButton)
        
        var view4  = UIView(frame: CGRectMake(0, screenHeight/2+240, screenWidth+4, screenHeight-60))
        view4.backgroundColor = UIColor.whiteColor()
        view4.layer.borderWidth = 2
        view4.layer.borderColor = UIColor.grayColor().CGColor
        view.addSubview(view4)
        
        let view4Label1 = UILabel()
        view4Label1.text = " MinFare \n Rs 25 for 2km"
        view4Label1.font = UIFont(name: "MarkerFelt-Thin", size: 14)
        view4Label1.textColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:1.0)
        //view4Label1.textAlignment = .Center
        view4Label1.numberOfLines = 5
        // view1Label.backgroundColor = UIColor.grayColor()
        view4Label1.frame = CGRectMake(10,15,100, 50)
        view4.addSubview(view4Label1)

        let view4Label2 = UILabel()
        view4Label2.text = " Fare (after 2km)\n Rs 6 per km + Rs 1 per min"
        view4Label2.font = UIFont(name: "MarkerFelt-Thin", size: 14)
        view4Label2.textColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:1.0)
       // view4Label2.textAlignment = .Center
        view4Label2.numberOfLines = 5
        // view1Label.backgroundColor = UIColor.grayColor()
        view4Label2.frame = CGRectMake(140,15,170, 50)
        view4.addSubview(view4Label2)

        let infoIconbutton = UIButton()
        let infoimage = UIImage(named: "info_icon_onclick.png") as UIImage?
        infoIconbutton.setImage(infoimage, forState: .Normal)
        infoIconbutton.frame = CGRectMake(320,15, 50, 50)
        infoIconbutton.addTarget(self, action: "infoPressed", forControlEvents: .TouchUpInside)
        view4.addSubview(infoIconbutton)
    }
    
    /*func menuPressed(){
        // getAutobutton.alpha = 0
        //gpsButton.alpha = 0
        //assigningButton.alpha = 0
        // cancelButton.alpha = 0
        // calldriverButton.alpha = 0
        //view4.alpha = 0
        //menuView.alpha = 1
        UIView.animateWithDuration(0.2, animations: {
            self.menuView.center = CGPointMake(self.menuView.center.x+260, self.menuView.center.y)
        })
    }*/

}
