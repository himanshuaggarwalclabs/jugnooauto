//
//  HelpViewController.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 03/03/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var helpArr : [String] = ["Send Us an Email","Call Us","FAQs","About Jugnoo","Terms of Use","Privacy Policy"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.creatingHelpscreen()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func creatingHelpscreen(){
        
        var view1  = UIView(frame: CGRectMake(0, screenHeight/2-333.5, screenWidth, 80))
        view1.backgroundColor = UIColor(red:36.0/255.0, green:41.0/255.0,blue:48.0/255.0,alpha:1.0)
        self.view.addSubview(view1)
        
        
        let label1 = UILabel()
        label1.text = "Help"
        label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        label1.textAlignment = .Center
        //label1.numberOfLines = 5
        label1.frame = CGRectMake(screenWidth/2-62, screenHeight/2-300, 150, 30)
        self.view.addSubview(label1)
        
        
        let backButton = UIButton()
        //backButton.setTitle("Back", forState: .Normal)
        let image = UIImage(named: "back-button.png") as UIImage?
        backButton.setImage(image, forState: .Normal)
        //backButton.backgroundImageForState("back-button.png")
        //backButton.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        //backButton.backgroundColor = UIColor.cyanColor()
        //backButton.layer.cornerRadius = 5
        backButton.frame = CGRectMake(screenWidth/2-190,screenHeight/2-310, 50, 50)
        backButton.addTarget(self, action: "BackPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(backButton)
        
        
        var helpTableView: UITableView  =   UITableView()
        helpTableView.frame         =   CGRectMake(0,screenHeight-600, screenWidth, screenHeight-400);
        helpTableView.delegate      =   self
        helpTableView.dataSource    =   self
        
        helpTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        //couponTableView.backgroundColor = UIColor.cyanColor()
        self.view.addSubview(helpTableView)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return helpArr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
            cell.textLabel?.text = helpArr[indexPath.row]
            return cell
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let height: CGFloat = 40
        return height
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("You selected cell #\(indexPath.row)!")
        if indexPath.row == 2 {
            let faqView = self.storyboard?.instantiateViewControllerWithIdentifier("faqView") as FAQsViewController
            self.navigationController?.pushViewController(faqView, animated: true)

        }
    }


    
    
    func BackPressed(){
        navigationController!.popToViewController(navigationController!.viewControllers[2] as UIViewController, animated: true)
    }

}
