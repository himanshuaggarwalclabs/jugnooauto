//
//  RidesViewController.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 03/03/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit

class RidesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var ridesTableView: UITableView  =   UITableView()
    var futureTableView: UITableView = UITableView()
    var viewline: UIView = UIView()
    var viewline2: UIView = UIView()
    //var viewline : UIView = UIView(frame: CGRectMake(0, screenHeight-545, screenWidth, 1))
    override func viewDidLoad() {
        super.viewDidLoad()

        self.creatingRidescreen()
        self.ridesTableView.alpha = 1
        self.ridesTableView.tag  = 1
        self.futureTableView.tag = 2
        self.futureTableView.alpha = 0
        self.viewline.alpha = 1
        self.viewline2.alpha = 0

        // Do any additional setup after loading the view.
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    func creatingRidescreen(){
        var view1  = UIView(frame: CGRectMake(0, screenHeight/2-333.5, screenWidth, 80))
        view1.backgroundColor = UIColor(red:36.0/255.0, green:41.0/255.0,blue:48.0/255.0,alpha:1.0)
        self.view.addSubview(view1)
        
        var viewDividerline = UIView(frame: CGRectMake(0, screenHeight-545, screenWidth, 1))
        viewDividerline.backgroundColor = UIColor.blackColor()
        self.view.addSubview(viewDividerline)
        
        viewline = UIView(frame: CGRectMake(screenWidth/2-150, screenHeight-547, 120, 2))
        viewline.backgroundColor = UIColor.blackColor()
        self.view.addSubview(viewline)
        
        viewline2 = UIView(frame: CGRectMake(screenWidth/2+30, screenHeight-547, 120, 2))
        viewline2.backgroundColor = UIColor.blackColor()
        self.view.addSubview(viewline2)

        
        let label1 = UILabel()
        label1.text = "Rides"
        label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label1.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        label1.textAlignment = .Center
        //label1.numberOfLines = 5
        label1.frame = CGRectMake(screenWidth/2-62, 40, 150, 30)
        view1.addSubview(label1)
        
        let ridesButton = UIButton()
        ridesButton.setTitle("Ride History", forState: .Normal)
        ridesButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        //ridesButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        //ridesButton.layer.cornerRadius = 5
        ridesButton.frame = CGRectMake(screenWidth/2-150,screenHeight/2-250, 120, 35)
        ridesButton.addTarget(self, action: "ridePressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(ridesButton)
        
        let futureButton = UIButton()
        futureButton.setTitle("Future Rides", forState: .Normal)
        futureButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        //ridesButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        //futureButton.layer.cornerRadius = 5
        futureButton.frame = CGRectMake(screenWidth/2+30,screenHeight/2-250, 120, 35)
        futureButton.addTarget(self, action: "futurePressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(futureButton)

        
        let backButton = UIButton()
        //backButton.setTitle("Back", forState: .Normal)
        let image = UIImage(named: "back-button.png") as UIImage?
        backButton.setImage(image, forState: .Normal)
        //backButton.backgroundImageForState("back-button.png")
        //backButton.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        //backButton.backgroundColor = UIColor.cyanColor()
        //backButton.layer.cornerRadius = 5
        backButton.frame = CGRectMake(screenWidth/2-190,30, 50, 50)
        backButton.addTarget(self, action: "BackPressed", forControlEvents: .TouchUpInside)
        view1.addSubview(backButton)
        
        ridesTableView.frame         =   CGRectMake(0,screenHeight-535, screenWidth, screenHeight+50);
        ridesTableView.delegate      =   self
        ridesTableView.dataSource    =   self
        
        ridesTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        //couponTableView.backgroundColor = UIColor.cyanColor()
        self.view.addSubview(ridesTableView)
        
        
        futureTableView.frame         =   CGRectMake(0,screenHeight-535, screenWidth, screenHeight+50);
        futureTableView.delegate      =   self
        futureTableView.dataSource    =   self
        
        futureTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        //couponTableView.backgroundColor = UIColor.cyanColor()
        self.view.addSubview(futureTableView)


    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1{
            return 2
        }
        if tableView == 2 {
            return 1
        }
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
        
        
        if tableView.tag == 1{
            
            var cellView = UIView(frame: CGRectMake(2, 2, screenWidth, 120))
            //cellView.backgroundColor = UIColor.redColor()
            cell.contentView.addSubview(cellView)
            
            // var distanceImage = UIImage(named:"coupon_account.png" )
            var cellDistanceimage = UIImageView(frame: CGRectMake(5, 175, 15, 15))
            cellDistanceimage.image = UIImage(named: "distance_Image.png")
            //cellDistanceimage.backgroundColor = UIColor.greenColor()
            cellView.addSubview(cellDistanceimage)

            
            // var distanceImage = UIImage(named:"coupon_account.png" )
            var cellTimeimage = UIImageView(frame: CGRectMake(100, 175, 15, 15))
            cellTimeimage.image = UIImage(named: "time_image.png")
           // cellTimeimage.backgroundColor = UIColor.greenColor()
            cellView.addSubview(cellTimeimage)
            
            let cellLabel1 = UILabel()
            cellLabel1.text = " From :"
            cellLabel1.font = UIFont(name: "MarkerFelt-Thin", size: 20)
            cellLabel1.textColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:1.0)
            //label3.textColor = UIColor.blackColor()
            //label3.textAlignment = .Center
            //cellLabel1.numberOfLines = 5
            cellLabel1.frame = CGRectMake(5, 20, 80, 30)
            cellView.addSubview(cellLabel1)
            
            let cellLabel2 = UILabel()
            cellLabel2.text = " To :"
            cellLabel2.font = UIFont(name: "MarkerFelt-Thin", size: 25)
            cellLabel2.textColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:1.0)
            //label3.textColor = UIColor.blackColor()
            //label3.textAlignment = .Center
            //cellLabel2.numberOfLines = 5
            cellLabel2.frame = CGRectMake(10, 100, 80, 30)
            cellView.addSubview(cellLabel2)
            
           let cellLabel3 = UILabel()
           // cellLabel3.text = " To :"
            //cellLabel3.font = UIFont(name: "MarkerFelt-Thin", size: 25)
            //cellLabel3.textColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:1.0)
            //label3.textColor = UIColor.blackColor()
            //label3.textAlignment = .Center
            //cellLabel2.numberOfLines = 5
            cellLabel3.frame = CGRectMake(60, 20, 200, 70)
            cellLabel3.backgroundColor = UIColor.grayColor()
            cellView.addSubview(cellLabel3)

            
            let cellLabel4 = UILabel()
            // cellLabel3.text = " To :"
            //cellLabel3.font = UIFont(name: "MarkerFelt-Thin", size: 25)
            //cellLabel3.textColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:1.0)
            //label3.textColor = UIColor.blackColor()
            //label3.textAlignment = .Center
            //cellLabel2.numberOfLines = 5
            cellLabel4.frame = CGRectMake(60, 100, 200, 70)
            cellLabel4.backgroundColor = UIColor.grayColor()
            cellView.addSubview(cellLabel4)
            
            let cellDistancelabel = UILabel()
            // cellLabel3.text = " To :"
            //cellLabel3.font = UIFont(name: "MarkerFelt-Thin", size: 25)
            //cellLabel3.textColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:1.0)
            //label3.textColor = UIColor.blackColor()
            //label3.textAlignment = .Center
            //cellLabel2.numberOfLines = 5
            cellDistancelabel.frame = CGRectMake(25, 175, 20, 20)
            cellDistancelabel.backgroundColor = UIColor.greenColor()
            cellView.addSubview(cellDistancelabel)

            let cellTimelabel = UILabel()
            // cellLabel3.text = " To :"
            //cellLabel3.font = UIFont(name: "MarkerFelt-Thin", size: 25)
            //cellLabel3.textColor = UIColor(red:0.0/255.0, green:0.0/255.0,blue:0.0/255.0,alpha:1.0)
            //label3.textColor = UIColor.blackColor()
            //label3.textAlignment = .Center
            //cellLabel2.numberOfLines = 5
            cellTimelabel.frame = CGRectMake(120, 175, 150, 20)
            cellTimelabel.backgroundColor = UIColor.greenColor()
            cellView.addSubview(cellTimelabel)

            let cellFairlabel = UILabel()
            cellFairlabel.text = " Rs."
            cellFairlabel.font = UIFont(name: "MarkerFelt-Thin", size: 25)
            cellFairlabel.textColor = (UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0))
            //label3.textColor = UIColor.blackColor()
            //label3.textAlignment = .Center
            //cellLabel2.numberOfLines = 5
            cellFairlabel.frame = CGRectMake(290, 100, 70, 30)
            cellFairlabel.backgroundColor = UIColor.redColor()
            cellView.addSubview(cellFairlabel)

            return cell
            
        }
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let height: CGFloat = 200
        return height
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("You selected cell #\(indexPath.row)!")
    }
    
    
    func ridePressed(){
        ridesTableView.alpha = 1
        futureTableView.alpha = 0
        viewline.alpha = 1
        viewline2.alpha = 0
        
    }
    func futurePressed(){
        ridesTableView.alpha = 0
        futureTableView.alpha = 1
        viewline.alpha = 0
        viewline2.alpha = 1
    }

    func BackPressed(){
        
        navigationController!.popToViewController(navigationController!.viewControllers[2] as UIViewController, animated: true)
    }
    

   }
