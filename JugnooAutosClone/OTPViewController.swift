//
//  OTPViewController.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 11/03/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit

class OTPViewController: UIViewController {

  var oneTimepassword = ""
  var otpFlag = ""
  var otpField = UITextField(frame:CGRectMake(screenWidth/2-165, screenHeight/2-190, 330, 50))
  
    override func viewDidLoad() {
        super.viewDidLoad()

        self.creatingOtpscreen()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func creatingOtpscreen(){
        
        splashBgimageView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        view.addSubview(splashBgimageView)

        
        let label1 = UILabel()
        label1.text = "Verification"
        label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label1.textColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        label1.textAlignment = .Center
        //label1.numberOfLines = 5
        label1.frame = CGRectMake(screenWidth/2-95, screenHeight/2-300, 200, 30)
        self.view.addSubview(label1)

        let label2 = UILabel()
        label2.text = "Please enter the One Time Password you just\n   received via SMS at +917508959837"
        label2.font = UIFont(name: "MarkerFelt-Thin", size: 18)
        label2.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        //label2.textAlignment = .Center
        label2.numberOfLines = 6
        label2.frame = CGRectMake(screenWidth/2-150, screenHeight/2-250, screenWidth-50, 50)
        self.view.addSubview(label2)
        
        let label3 = UILabel()
        label3.text = " Didn't receive the password on SMS yet?\nWe will give you a call to let you know your \n                   One Time Password"
        label3.font = UIFont(name: "MarkerFelt-Thin", size: 18)
        label3.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        //label2.textAlignment = .Center
        label3.numberOfLines = 8
        label3.frame = CGRectMake(screenWidth/2-150, screenHeight/2+240, screenWidth-50, 70)
        self.view.addSubview(label3)
        
        //var otpField = UITextField(frame:CGRectMake(screenWidth/2-165, screenHeight/2-190, 330, 50))
        otpField.backgroundColor = UIColor.whiteColor()
        otpField.borderStyle = UITextBorderStyle.Line
        //emailField = UITextField (frame:CGRectMake(screenWidth/2, screenHeight/2, 200, 30));
        // emailField.layer.cornerRadius = 5
        otpField.placeholder = "Enter One Time Password"
     // otpField.tag = 505
        otpField.borderStyle = .RoundedRect
        self.view.addSubview(otpField)

        let confirmButton = UIButton()
        confirmButton.setTitle("CONFIRM", forState: .Normal)
        confirmButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        confirmButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        confirmButton.layer.cornerRadius = 5
        confirmButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2-120, 330, 50)
        confirmButton.addTarget(self, action: "confirmPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(confirmButton)

        let callMebutton = UIButton()
        callMebutton.setTitle("CALL ME", forState: .Normal)
        callMebutton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        callMebutton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        callMebutton.layer.cornerRadius = 5
        callMebutton.frame = CGRectMake(screenWidth/2-80,screenHeight/2+180, 170, 50)
        callMebutton.addTarget(self, action: "callmePressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(callMebutton)
        
        let backButton = UIButton()
        backButton.setTitle("Back", forState: .Normal)
        backButton.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        //backButton.backgroundColor = UIColor.cyanColor()
        //backButton.layer.cornerRadius = 5
        backButton.frame = CGRectMake(screenWidth/2-200,screenHeight/2-300, 100, 30)
        backButton.addTarget(self, action: "backPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(backButton)
        
        }
    func backPressed(){
        navigationController!.popToViewController(navigationController!.viewControllers[1] as UIViewController, animated: true)
    }
    func confirmPressed(){
      
      println("confirmPressed")
      oneTimepassword = otpField.text
      
      var responseFlag = ""
      var responsePopup = ""
      var serverStatus = ""
      var serverAuthkey = ""
      var serverCUS = ""
      var userEmail = ""
      var userPhonenumber = ""
      var userReferralcode = ""
      var userImage = ""
      var userName = ""
      
      
      
      
      println(oneTimepassword)
      if oneTimepassword == "" {
        
        let errorAlert = UIAlertView(title: "Sign up Failure", message: "Please Enter a valid OTP", delegate: self, cancelButtonTitle: "OK")
        errorAlert.show()
        
      }else {
        
        var systemVersion = UIDevice.currentDevice().systemVersion
        var serverRespnseMessage = ""
        
        
        var actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake((screenWidth / 2) - 25,screenHeight - 200, 50, 50)) as UIActivityIndicatorView
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        actInd.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        self.view.addSubview(actInd)
        actInd.startAnimating()
        
        let manager = AFHTTPRequestOperationManager()
        manager.requestSerializer.setValue("608c6c08443c6d933576b90966b727358d0066b4", forHTTPHeaderField: "X-Auth-Token")
        
        manager.securityPolicy.allowInvalidCertificates = true
        
        var parameterss:NSDictionary = ["email":userInputemail,
          "password":userInputpassword,
          "device_token":myDeviceToken,
          "device_type":"1",
          "device_name":"iPhone",
          "app_version":"101",
          "os_version":systemVersion,
          "country":"India",
          "unique_device_id":"",
          "client_id":clientIdentifier,
          "otp":oneTimepassword]
        
        println("passing parameters")
        println(parameterss)
        // https://54.81.229.172:8012/
        // var parameterss = [“user”:”admin”,”password”:”123456"]


        manager.POST( "https://www.test.jugnoo.in:8012/verify_otp",
          parameters: parameterss,
          success: {
            (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
            var resultDesp:String = responseObject.description
            println(resultDesp)
            
            actInd.stopAnimating()
            
            let APIflag: AnyObject? = responseObject.valueForKey("flag")
            responseFlag = "\((APIflag)!)"
            
            let APIpopup: AnyObject? = responseObject.valueForKey("update_popup")
            responsePopup = "\((APIpopup)!)"
            
            let APIstatus: AnyObject? = responseObject.valueForKey("email_verification_status")
            serverStatus = "\((APIstatus)!)"

            let APIauthkey: AnyObject? = responseObject.valueForKey("auth_key")
            serverStatus = "\((APIauthkey)!)"
            
            let APIcus: AnyObject? = responseObject.valueForKey("current_user_status")
            serverCUS = "\((APIcus)!)"
            
            let APIemail: AnyObject? = responseObject.valueForKey("user_email")
            userEmail = "\((APIemail)!)"
            
            let APIphoneno: AnyObject? = responseObject.valueForKey("phone_no")
            userPhonenumber = "\((APIphoneno)!)"
            
            let APIreferralcode: AnyObject? = responseObject.valueForKey("referral_code")
            userReferralcode = "\((APIreferralcode)!)"
            
            let APIimage: AnyObject? = responseObject.valueForKey("user_image")
            userImage = "\((APIimage)!)"
            
            let APIname: AnyObject? = responseObject.valueForKey("user_name")
            userName = "\((APIname)!)"
            
            /*let APIerror: AnyObject? = responseObject.valueForKey("fatal error")
            var error = "\((APIerror)!)"*/

            
           // var flagArray = resultDesp.componentsSeparatedByString("flag = ")
            if NSString(string: resultDesp).containsString("flag = ") {
              //var newflagArray = flagArray[1].componentsSeparatedByString(";")
              //var responseFlag = newflagArray[0]
              println("\(responseFlag)")
              self.otpFlag = responseFlag
            }
            //--------------------
            //var popupArray = resultDesp.componentsSeparatedByString("popup = ")
            if NSString(string: resultDesp).containsString("update_popup = ") {
              //var newPopupArray = popupArray[1].componentsSeparatedByString(";")
              //var responsePopup = newPopupArray[0]
              println(" PopUp = \(responsePopup)")
            }
            //--------------------
            //var statusArray = resultDesp.componentsSeparatedByString("status = ")
            if NSString(string: resultDesp).containsString("email_verification_status = ") {
              //var newStatusArray = statusArray[1].componentsSeparatedByString(";")
              //var serverStatus = newStatusArray[0]
              println(" Status =  \(serverStatus)")
            }
            //--------------------
            //var authKeyArray = resultDesp.componentsSeparatedByString("\"auth_key\" = ")
            if NSString(string: resultDesp).containsString("auth_key = ") {
              //var newAuthKeyArray = authKeyArray[1].componentsSeparatedByString(";")
              //var serverAuthKey = newAuthKeyArray[0]
              println(" Authkey =  \(serverAuthkey)")
              var TempAccessToken = serverAuthkey  + clientSharedSecret
              
              accessToken = self.sha256(TempAccessToken)
              println(accessToken)

              
              NSUserDefaults.standardUserDefaults().setObject(accessToken, forKey: "token")
              NSUserDefaults.standardUserDefaults().synchronize()
              var userAccessToken: AnyObject = NSUserDefaults.standardUserDefaults().valueForKey("token")!
              
              
              // Set KeyChain
              var userAuthKeyString = "\(serverAuthkey)"
              UICKeyChainStore.setString("Jugnoo_", forKey: JUGNOO_AUTH_KEY, service: JUGNOO_KEYCHAIN_SERVICE)
              
            }
            
              //--------------------
              //var cUSArray = resultDesp.componentsSeparatedByString("\"current_user_status\" = ")
              if NSString(string: resultDesp).containsString("current_user_status = ") {
               // var newCUSArray = cUSArray[1].componentsSeparatedByString(";")
                //var serverCUS = newCUSArray[0]
                println(" Current User Status =  \(serverCUS)")
              }
              //--------------------
              //var emailArray = resultDesp.componentsSeparatedByString("email = \"")
              if NSString(string: resultDesp).containsString("user_email = ") {
                //var newEmailArray = emailArray[1].componentsSeparatedByString("\";")
               // var serverEmail = newEmailArray[0]
                println(" Email =  \(userEmail)")
                //UserEmail = "\(serverEmail)"
              }
              //--------------------
              /*   var phoneArray = resultDesp.componentsSeparatedByString("\"phone_no\" = \"")
              if NSString(string: resultDesp).containsString("\"phone_no\" = \"") {
              var newPhoneArray = phoneArray[1].componentsSeparatedByString("\";")
              var serverPhone = newPhoneArray[0]
              println(" Phone =  \(serverPhone)")
              userPhoneNumber = "\(serverPhone)"
              } */
              
              //var phoneArray = resultDesp.componentsSeparatedByString("\"phone_no\" = ")
              if NSString(string: resultDesp).containsString("\"phone_no\" = ") {
                //var newPhoneArray = phoneArray[1].componentsSeparatedByString(";")
                //var serverPhone = newPhoneArray[0]
                println(" Phone =  \(userPhonenumber)")
                //userPhoneNumber = serverPhone
                //println(".......>>>>>>>")
                //println(userPhoneNumber)
               // println(".......>>>>>>>")
              }
              
              /*if NSString(string: resultDesp).containsString("\"phone_no\" = \"") {
                var phoneArray = resultDesp.componentsSeparatedByString("\"phone_no\" = \"")
                if NSString(string: resultDesp).containsString("\"phone_no\" = \"") {
                  var newPhoneArray = phoneArray[1].componentsSeparatedByString("\";")
                  var serverPhone = newPhoneArray[0]
                  println(" Phone =  \(serverPhone)")
                  userPhoneNumber = serverPhone
                  println(".......>>>>>>>")
                  println(userPhoneNumber)
                  println(".......>>>>>>>")
                }
              }*/
              
              //--------------------
              //var rCArray = resultDesp.componentsSeparatedByString("\"referral_code\" = ")
              if NSString(string: resultDesp).containsString("\"referral_code\" = ") {
                //var newRCArray = rCArray[1].componentsSeparatedByString(";")
                //var serverRC = newRCArray[0]
                println(" Refral Code =  \(userReferralcode)")
                //userRefralCode = serverRC
              }
              //--------------------
              //var imageArray = resultDesp.componentsSeparatedByString("\"user_image\" = \"")
              if NSString(string: resultDesp).containsString("\"user_image\" = \"") {
                //var newImageArray = imageArray[1].componentsSeparatedByString("\";")
                //var serverImage = newImageArray[0]
                //userImagemageURL = serverImage
                println(" Image URL =  \(userImage)")
              }
              //--------------------
              /*      var userNameArray = resultDesp.componentsSeparatedByString("\"user_name\" = ")
              if NSString(string: resultDesp).containsString("\"user_name\" = ") {
              var newUserNameArray = userNameArray[1].componentsSeparatedByString(";")
              var serverUserName = newUserNameArray[0]
              userName = serverUserName
              println("\(serverUserName)")
              } */
              
             // var userNameArray = resultDesp.componentsSeparatedByString("\"user_name\" = ")
              if NSString(string: resultDesp).containsString("\"user_name\" = ") {
               // var newUserNameArray = userNameArray[1].componentsSeparatedByString(";")
                //var serverUserName = newUserNameArray[0]
                //userName = serverUserName
                println(" User Name (shankar) =  \(userName)")
              }
              
              /*if NSString(string: resultDesp).containsString("\"user_name\" = \"") {
                var userNameArray = resultDesp.componentsSeparatedByString("\"user_name\" = \"")
                if NSString(string: resultDesp).containsString("\"user_name\" = \"") {
                  var newUserNameArray = userNameArray[1].componentsSeparatedByString("\";")
                  var serverUserName = newUserNameArray[0]
                  userName = serverUserName
                  println(" User Name (shankar) =  \(serverUserName)")
                }
              }*/
              
              //--------------------
             /* //var respnseMessageArray = resultDesp.componentsSeparatedByString("error = \"")
              if NSString(string: resultDesp).containsString("error = \"") {
               // var newRespnseMessageArray = respnseMessageArray[1].componentsSeparatedByString(";")
                //var respnseMessage = newRespnseMessageArray[0]
                //serverRespnseMessage = respnseMessage
                println("Error= \(error)")
              //}*/



            if self.otpFlag == "407" {
              
              let testView = self.storyboard?.instantiateViewControllerWithIdentifier("testView") as CustomerLoginViewController
              self.navigationController?.pushViewController(testView, animated: true)
            }
            
            if self.otpFlag != "407" {
              let errorAlert = UIAlertView(title: "Sign up Failure", message: serverRespnseMessage, delegate: self, cancelButtonTitle: "OK")
              errorAlert.show()
            }

            
            
          },
          failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
            println("Error: " + error.description)
            
            actInd.stopAnimating()
            let errorAlert = UIAlertView(title: "Network Error", message: error.localizedDescription, delegate: self, cancelButtonTitle: "OK")
            errorAlert.show()
        })
        
      }
      

      
    }
  
    func callmePressed(){
        println("callmePressed")
    }
  
  //------SHA256 Function----------
  func sha256(string: NSString) -> NSString {
    let data = string.dataUsingEncoding(NSUTF8StringEncoding)!
    var hash = [UInt8](count: Int(CC_SHA256_DIGEST_LENGTH), repeatedValue: 0)
    CC_SHA256(data.bytes, CC_LONG(data.length), &hash)
    let resstr = NSMutableString()
    for byte in hash {
      resstr.appendFormat("%02hhx", byte)
    }
    return resstr
  }


    }
