//
//  RegisterViewController.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 26/02/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit
import MapKit

//Sign Up Form strings
var userInputname = ""
var userInputemail = ""
var userInputphoneNumber = ""
var userInputpassword = ""
var userPromocode = ""

class RegisterViewController: UIViewController, CLLocationManagerDelegate {

  
  /*//Sign Up Form strings
  var userInputname = ""
  var userInputemail = ""
  var userInputphoneNumber = ""
  var userInputpassword = ""
  var userPromocode = ""*/
  
  // Fetching User location
  var userLatitude: CLLocationDegrees = 30.718821
  var userLongitude: CLLocationDegrees = 76.810803
  var locationManager = CLLocationManager()

  func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    // println("location: \(locations)")
    
    var userLocation: CLLocation = locations[0] as CLLocation
    
    userLatitude = userLocation.coordinate.latitude
    userLongitude = userLocation.coordinate.latitude
  }
  
  func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
    // println(error)
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.creatingRegisterscreen()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func creatingRegisterscreen(){
        
        let splashBgimage = "jugnoo_splash.jpg"
        let bgImage = UIImage(named: splashBgimage)
        let splashBgimageView = UIImageView(image: bgImage!)
        splashBgimageView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        view.addSubview(splashBgimageView)
        
        var view1  = UIView(frame: CGRectMake(screenWidth/2-165, screenHeight/2-165, 130, 1))
        view1.backgroundColor = UIColor.blackColor()
        self.view.addSubview(view1)
        var view2  = UIView(frame: CGRectMake(screenWidth/2+40, screenHeight/2-165, 130, 1))
        view2.backgroundColor = UIColor.blackColor()
        self.view.addSubview(view2)
        
        let label1 = UILabel()
        label1.text = "Register"
        label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label1.textColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        label1.textAlignment = .Center
        //label1.numberOfLines = 5
        label1.frame = CGRectMake(screenWidth/2-40, screenHeight/2-300, 100, 30)
        self.view.addSubview(label1)
        let label2 = UILabel()
        label2.text = "OR"
        label2.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label2.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        label2.textAlignment = .Center
        //label1.numberOfLines = 5
        label2.frame = CGRectMake(screenWidth/2-45, screenHeight/2-180, 100, 30)
        self.view.addSubview(label2)
        
        var nameField = UITextField(frame:CGRectMake(screenWidth/2-165, screenHeight/2-140, 330, 60))
        nameField.backgroundColor = UIColor.whiteColor()
        nameField.borderStyle = UITextBorderStyle.Line
        //emailField = UITextField (frame:CGRectMake(screenWidth/2, screenHeight/2, 200, 30));
        // emailField.layer.cornerRadius = 5
        nameField.placeholder = "NAME"
      nameField.tag = 500
        nameField.borderStyle = .RoundedRect
        self.view.addSubview(nameField)

        var phoneField = UITextField(frame:CGRectMake(screenWidth/2-165, screenHeight/2-20, 330, 60))
        phoneField.backgroundColor = UIColor.whiteColor()
        phoneField.borderStyle = UITextBorderStyle.Line
        //emailField = UITextField (frame:CGRectMake(screenWidth/2, screenHeight/2, 200, 30));
        // emailField.layer.cornerRadius = 5
        phoneField.placeholder = "PHONE NUMBER"
      phoneField.tag = 501
        phoneField.borderStyle = .RoundedRect
        self.view.addSubview(phoneField)

        
        var emailField = UITextField(frame:CGRectMake(screenWidth/2-165, screenHeight/2-80, 330, 60))
        emailField.backgroundColor = UIColor.whiteColor()
        emailField.borderStyle = UITextBorderStyle.Line
        //emailField = UITextField (frame:CGRectMake(screenWidth/2, screenHeight/2, 200, 30));
        // emailField.layer.cornerRadius = 5
        emailField.placeholder = "Email ID"
      emailField.tag = 502
        emailField.borderStyle = .RoundedRect
        self.view.addSubview(emailField)
        
        var passwordField = UITextField(frame:CGRectMake(screenWidth/2-165, screenHeight/2+40, 330, 60))
        passwordField.backgroundColor = UIColor.whiteColor()
        passwordField.borderStyle = UITextBorderStyle.Line
        //emailField = UITextField (frame:CGRectMake(screenWidth/2, screenHeight/2, 200, 30));
        // emailField.layer.cornerRadius = 5
        passwordField.placeholder = "Password"
      passwordField.tag = 503
        passwordField.borderStyle = .RoundedRect
        self.view.addSubview(passwordField)

        var cpasswordField = UITextField(frame:CGRectMake(screenWidth/2-165, screenHeight/2+100, 330, 60))
        cpasswordField.backgroundColor = UIColor.whiteColor()
        cpasswordField.borderStyle = UITextBorderStyle.Line
        //emailField = UITextField (frame:CGRectMake(screenWidth/2, screenHeight/2, 200, 30));
        // emailField.layer.cornerRadius = 5
        cpasswordField.placeholder = "Confirm Password"
      cpasswordField.tag = 504
        cpasswordField.borderStyle = .RoundedRect
        self.view.addSubview(cpasswordField)

        var promocodeField = UITextField(frame:CGRectMake(screenWidth/2-165, screenHeight/2+160, 330, 60))
        promocodeField.backgroundColor = UIColor.whiteColor()
        promocodeField.borderStyle = UITextBorderStyle.Line
        //emailField = UITextField (frame:CGRectMake(screenWidth/2, screenHeight/2, 200, 30));
        // emailField.layer.cornerRadius = 5
        promocodeField.placeholder = "Referral/Promo code (Optional) "
      promocodeField.tag = 505
        promocodeField.borderStyle = .RoundedRect
        self.view.addSubview(promocodeField)

        
        
        
        let backButton = UIButton()
        backButton.setTitle("Back", forState: .Normal)
        backButton.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        //backButton.backgroundColor = UIColor.cyanColor()
        //backButton.layer.cornerRadius = 5
        backButton.frame = CGRectMake(screenWidth/2-200,screenHeight/2-300, 100, 30)
        backButton.addTarget(self, action: "backPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(backButton)
        
        let registerwfbButton = UIButton()
        registerwfbButton.setTitle("Register With FACEBOOK", forState: .Normal)
        registerwfbButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        registerwfbButton.backgroundColor = UIColor(red:60.0/255.0, green:90.0/255.0,blue:154.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        registerwfbButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2-250, 330, 60)
        registerwfbButton.addTarget(self, action: "registerwfbPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(registerwfbButton)
        
        let registerButton = UIButton()
        registerButton.setTitle("Register", forState: .Normal)
        registerButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        registerButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        registerButton.layer.cornerRadius = 5
        registerButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2+240, 330, 60)
        registerButton.addTarget(self, action: "registerPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(registerButton)
      
      //--------------------*
      
      //------------------------*
    }
    func backPressed(){
        self.navigationController?.popToRootViewControllerAnimated(true)

    }
    func registerwfbPressed(){
        println("Register With FB Password")
    }
  
  //----------------*
    func registerPressed(){
      
        println("Register Password")
      
      var signUpFlag = ""
      var signMsg = ""
      
      var systemVersion = UIDevice.currentDevice().systemVersion
      
      for index in 500..<505 {
        
        var textField = UITextField()
        textField = view.viewWithTag(index) as UITextField
        
        if textField.tag == 500 {
          userInputname = textField.text
        }
        if textField.tag == 501 {
          userInputphoneNumber = textField.text
        }
        if textField.tag == 502 {
          userInputemail = textField.text
        }
        if textField.tag == 503 {
          userInputpassword = textField.text
        }
        if textField.tag == 504 {
          userPromocode = textField.text
        }
      }
      
      //-----------*
      if userInputname == "" || userInputemail == "" || userInputpassword == "" || userInputphoneNumber == "" {
        
        let errorAlert = UIAlertView(title: "Incomplete Details", message: "Please make sure you fill in all the fields before you sign up.", delegate: self, cancelButtonTitle: "OK")
        errorAlert.show()
        
      }else {
        
        if userInputphoneNumber.utf16Count != 10 {
          let errorAlert = UIAlertView(title: "Invalid Phone Number", message: "Please enter a 10 digit phone number.", delegate: self, cancelButtonTitle: "OK")
          errorAlert.show()
        } else {
          
          var checkEmail = self.isValidEmail(userInputemail)
          
          if checkEmail == false {
            let errorAlert = UIAlertView(title: "Invalid Email ID", message: "Please enter a valid email id", delegate: self, cancelButtonTitle: "OK")
            errorAlert.show()
        //--------------*
          } else {


      let manager = AFHTTPRequestOperationManager()
      manager.requestSerializer.setValue("608c6c08443c6d933576b90966b727358d0066b4", forHTTPHeaderField: "X-Auth-Token")
      
      manager.securityPolicy.allowInvalidCertificates = true
      /* var parameter: NSDictionary = ["email":"gagandeep@jugnoo.in",
      "password":"123456"]*/
      
      var parameterss:NSDictionary = ["user_name":userInputname,
      "phone_no":"+91" + userInputphoneNumber,
      "email":userInputemail,
      "password":userInputpassword,
      "latitude":userLatitude,
      "longitude":userLongitude,
      "device_token":myDeviceToken,
      "device_type":"1",
      "device_name":"iPhone",
      "app_version":"101",
      "os_version":systemVersion,
      "country":"India",
      "unique_device_id":"",
      "referral_code":userPromocode,
      "client_id":clientIdentifier]
      
      
      
      manager.POST( "https://www.test.jugnoo.in:8012/register_using_email",
      parameters: parameterss,
      success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
        
    //******
      //println("JSON: " + responseObject.description)
     // println("-----------")
      let APIflag: AnyObject? = responseObject.valueForKey("flag")
        signUpFlag = "\((APIflag)!)"
        
      let APIerror: AnyObject? = responseObject.valueForKey("error")
        signMsg = "\((APIerror)!)"
      // println("\((APIflag)!)")
      //println((APIflag)!)
    //******
       
        var resultDesp:String = responseObject.description
        println(resultDesp)
        
        //var flagArray = resultDesp.componentsSeparatedByString("flag = ")
        if NSString(string: resultDesp).containsString("flag = ") {
        //  var newFlagArray = flagArray[1].componentsSeparatedByString(";")
        //  signUpFlag = newFlagArray[0]
          println(" signUp Flag = \(signUpFlag)")
        }
        //---------------------
        //var msgArray = resultDesp.componentsSeparatedByString("error = \"")
        if NSString(string: resultDesp).containsString("error = \"") {
          //var newMsgArray = msgArray[1].componentsSeparatedByString("\";")
          //signMsg = newMsgArray[0]
          println(" signUp Message = \(signMsg)")
        }
        
        if signUpFlag != "405" {
          println(" signUp Message = \(signMsg)")
          let errorAlert = UIAlertView(title: "Sign up Failure", message: signMsg, delegate: self, cancelButtonTitle: "OK")
          errorAlert.show()
        }
        if signUpFlag == "405" {
          
          let otpView = self.storyboard?.instantiateViewControllerWithIdentifier("otpView") as OTPViewController
          self.navigationController?.pushViewController(otpView, animated: true)

        }

        
        
      },
      failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
      println("Error: " + error.description)
        let errorAlert = UIAlertView(title: "Sign up Failure", message: error.localizedDescription, delegate: self, cancelButtonTitle: "OK")
        errorAlert.show()
      
      
      
      })
          }}}
      

      
    
      
  }
  
        func isValidEmail(testStr:String) -> Bool {
          println("validate calendar: \(testStr)")
          let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
          
          if let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx) {
            return emailTest.evaluateWithObject(testStr)
          }
          return false
        }
        

  
}
