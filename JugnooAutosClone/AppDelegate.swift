//
//  AppDelegate.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 24/02/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit
var myDeviceToken = ""
@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
  
  

    var window: UIWindow?
  
  func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData!) {
    println("Got token data! \(deviceToken)")
    
    
    var s1 = "\(deviceToken)"
    var ns1 = s1 as NSString
    var ns2 = ns1.stringByReplacingOccurrencesOfString("<", withString: "")
    var ns3 = ns2.stringByReplacingOccurrencesOfString(" ", withString: "")
    var ns4 = ns3.stringByReplacingOccurrencesOfString(">", withString: "")
    
    if ns4 != "" {
      myDeviceToken = ns4 as String
    }
  }

  
  
  
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
      
      var systemVersion = UIDevice.currentDevice().systemVersion
      var checkSystemVersion = systemVersion.toInt()
      
      if checkSystemVersion >= 8 {
        
        // Register for push in iOS 8
        let settings = UIUserNotificationSettings(forTypes: UIUserNotificationType.Alert | UIUserNotificationType.Sound | UIUserNotificationType.Badge, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
      } else {
        
        // Register for push in iOS 7
        UIApplication.sharedApplication().registerForRemoteNotifications()
      }

        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

