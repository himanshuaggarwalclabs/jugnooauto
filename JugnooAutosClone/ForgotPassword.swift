//
//  ForgotPassword.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 25/02/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit


class ForgotPassword: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.creatingForgotscreen()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func creatingForgotscreen(){
        
        splashBgimageView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        view.addSubview(splashBgimageView)
        
        let forgotJgimage = "jugnoo_text.png"
        let jgImage = UIImage(named: forgotJgimage)
        let forgotJgimageView = UIImageView(image: jgImage!)
        forgotJgimageView.frame = CGRect(x: screenWidth/2-150,y: screenHeight/2-250, width: 290, height: 90)
        view.addSubview(forgotJgimageView)

        let label1 = UILabel()
        label1.text = "FORGOT PASSWORD"
        label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label1.textColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        label1.textAlignment = .Center
        //label1.numberOfLines = 5
        label1.frame = CGRectMake(screenWidth/2-95, screenHeight/2-300, 200, 30)
        self.view.addSubview(label1)

        let label2 = UILabel()
        label2.text = "Please provide your email address to get \n instruction for reseting your password."
        label2.font = UIFont(name: "MarkerFelt-Thin", size: 18)
        label2.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        //label2.textAlignment = .Center
        label2.numberOfLines = 5
        label2.frame = CGRectMake(screenWidth/2-150, screenHeight/2-150, 300, 50)
        self.view.addSubview(label2)
        
        var emailField = UITextField(frame:CGRectMake(screenWidth/2-165, screenHeight/2-90, 330, 60))
        emailField.backgroundColor = UIColor.whiteColor()
        emailField.borderStyle = UITextBorderStyle.Line
        //emailField = UITextField (frame:CGRectMake(screenWidth/2, screenHeight/2, 200, 30));
        // emailField.layer.cornerRadius = 5
        emailField.placeholder = "Enter Email"
        emailField.borderStyle = .RoundedRect
        self.view.addSubview(emailField)
        
        let backButton = UIButton()
        backButton.setTitle("Back", forState: .Normal)
        backButton.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        //backButton.backgroundColor = UIColor.cyanColor()
        //backButton.layer.cornerRadius = 5
        backButton.frame = CGRectMake(screenWidth/2-200,screenHeight/2-300, 100, 30)
        backButton.addTarget(self, action: "BackPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(backButton)
        
        let sendButton = UIButton()
        sendButton.setTitle("Send Email", forState: .Normal)
        sendButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        sendButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        sendButton.layer.cornerRadius = 5
        sendButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2-10, 330, 60)
        sendButton.addTarget(self, action: "sendPressed", forControlEvents: .TouchUpInside)
        self.view.addSubview(sendButton)
        
  }
    func BackPressed(){
        //self.navigationController?.popToRootViewControllerAnimated(true)
        //self.navigationController?.popToViewController(LoginViewController(), animated: true)
        //var view = UIViewController(nibName: LoginViewController(), bundle: nil)
       // self.navigationController?.popToViewController(LoginViewController(), animated: true)
       // self.navigationController?.popToRootViewControllerAnimated(true)
        navigationController!.popToViewController(navigationController!.viewControllers[1] as UIViewController, animated: true)
    }
        
    func sendPressed(){
        println("SEND PRESSED")
    }
}
