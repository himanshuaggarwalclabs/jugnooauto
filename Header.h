//
//  Header.h
//  JugnooAutosClone
//
//  Created by Click Labs on 3/17/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

#import "AFNetworking.h"
#import <CoreFoundation/CoreFoundation.h>
#import "CommonCrypto/CommonDigest.h"
#import "UICKeyChainStore.h"
//#import <Foundation/Foundation.h>
//#import <SystemConfiguration/SystemConfiguration.h>
//#import <netinet/in.h>


typedef enum : NSInteger {
  NotReachable = 0,
  ReachableViaWiFi,
  ReachableViaWWAN
} NetworkStatus;



