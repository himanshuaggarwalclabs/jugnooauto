//
//  LoginViewController.swift
//  JugnooAutosClone
//
//  Created by Samar Singla on 24/02/15.
//  Copyright (c) 2015 Jugnoo. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.creatingLoginscreen()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func creatingLoginscreen(){
        
        let splashBgimage = "jugnoo_splash.jpg"
        let bgImage = UIImage(named: splashBgimage)
        let splashBgimageView = UIImageView(image: bgImage!)
        splashBgimageView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        view.addSubview(splashBgimageView)
        
        //-------------
        let label1 = UILabel()
        label1.text = "LOGIN"
        label1.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label1.textColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        label1.textAlignment = .Center
        //label1.numberOfLines = 5
        label1.frame = CGRectMake(screenWidth/2-40, screenHeight/2-300, 100, 30)
        self.view.addSubview(label1)
        
        let label2 = UILabel()
        label2.text = "OR"
        label2.font = UIFont(name: "MarkerFelt-Thin", size: 25)
        label2.textColor = UIColor(red:255.0/255.0, green:255.0/255.0,blue:255.0/255.0,alpha:1.0)
        label2.textAlignment = .Center
        //label1.numberOfLines = 5
        label2.frame = CGRectMake(screenWidth/2-45, screenHeight/2-180, 100, 30)
        self.view.addSubview(label2)
        
        //---------------
        let backButton = UIButton()
        backButton.setTitle("Back", forState: .Normal)
        backButton.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        //backButton.backgroundColor = UIColor.cyanColor()
        //backButton.layer.cornerRadius = 5
        backButton.frame = CGRectMake(screenWidth/2-200,screenHeight/2-300, 100, 30)
        backButton.addTarget(self, action: "backPressed:", forControlEvents: .TouchUpInside)
        self.view.addSubview(backButton)
        
        let loginvfbButton = UIButton()
        loginvfbButton.setTitle("LOGIN VIA FACEBOOK", forState: .Normal)
        loginvfbButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        loginvfbButton.backgroundColor = UIColor(red:60.0/255.0, green:90.0/255.0,blue:154.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        loginvfbButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2-250, 330, 60)
        loginvfbButton.addTarget(self, action: "loginvfbPressed:", forControlEvents: .TouchUpInside)
        self.view.addSubview(loginvfbButton)
        
        let loginButton = UIButton()
        loginButton.setTitle("LOGIN", forState: .Normal)
        loginButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        loginButton.backgroundColor = UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0)
        //backButton.layer.cornerRadius = 5
        loginButton.layer.cornerRadius = 5
        loginButton.frame = CGRectMake(screenWidth/2-165,screenHeight/2+5, 330, 60)
        loginButton.addTarget(self, action: "loginPressed:", forControlEvents: .TouchUpInside)
        self.view.addSubview(loginButton)
        
        let forgotButton = UIButton()
        forgotButton.setTitle("Forgot Password?", forState: .Normal)
        forgotButton.setTitleColor(UIColor(red:76.0/255.0, green:193.0/255.0,blue:210.0/255.0,alpha:1.0), forState: .Normal)
        forgotButton.frame = CGRectMake(screenWidth/2-100,screenHeight/2+75, 200, 40)
        forgotButton.addTarget(self, action: "forgotPressed:", forControlEvents: .TouchUpInside)
        self.view.addSubview(forgotButton)

        if screenHeight == 667 {
            
            var view1  = UIView(frame: CGRectMake(screenWidth/2-165, screenHeight/2-165, 130, 1))
            view1.backgroundColor = UIColor.blackColor()
            self.view.addSubview(view1)
            var view2  = UIView(frame: CGRectMake(screenWidth/2+40, screenHeight/2-165, 130, 1))
            view2.backgroundColor = UIColor.blackColor()
            self.view.addSubview(view2)
            
            
            var emailField = UITextField(frame:CGRectMake(screenWidth/2-165, screenHeight/2-140, 330, 60))
            emailField.backgroundColor = UIColor.whiteColor()
            emailField.borderStyle = UITextBorderStyle.Line
            
            emailField.placeholder = "Email ID"
            emailField.borderStyle = .RoundedRect
            self.view.addSubview(emailField)
            
            var passwordField = UITextField(frame:CGRectMake(screenWidth/2-165, screenHeight/2-82, 330, 60))
            passwordField.backgroundColor = UIColor.whiteColor()
            passwordField.borderStyle = UITextBorderStyle.Line
            
            passwordField.placeholder = "Password"
            passwordField.borderStyle = .RoundedRect
            self.view.addSubview(passwordField)
        }
        
        if screenHeight == 480 {
           
            splashBgimageView.frame = CGRect(x: 0, y: 0, width: 320, height: 480)
            var view1  = UIView(frame: CGRectMake(screenWidth/2-148, screenHeight/2-92, 110, 1))
            view1.backgroundColor = UIColor.blackColor()
            self.view.addSubview(view1)

            var view2  = UIView(frame: CGRectMake(screenWidth/2+23, screenHeight/2-92, 110, 1))
            view2.backgroundColor = UIColor.blackColor()
            self.view.addSubview(view2)
            
            label1.frame = CGRectMake(screenWidth/2-40, screenHeight/2-207, 85, 21)
            label2.frame = CGRectMake(screenWidth/2-45, screenHeight/2-100, 85, 21)
            backButton.frame = CGRectMake(screenWidth/2-173,screenHeight/2-207, 100, 30)
            loginvfbButton.frame = CGRectMake(screenWidth/2-145,screenHeight/2-160, 281.4, 43.4)
            loginButton.frame = CGRectMake(screenWidth/2-145,screenHeight/2+40, 281.4, 43.4)
            forgotButton.frame = CGRectMake(screenWidth/2-98,screenHeight/2+78, 200, 40)
            var emailField = UITextField(frame:CGRectMake(screenWidth/2-145, screenHeight/2-62, 281.4, 43.4))
            emailField.backgroundColor = UIColor.whiteColor()
            emailField.borderStyle = UITextBorderStyle.Line
            emailField.placeholder = "Email ID"
            emailField.borderStyle = .RoundedRect
            self.view.addSubview(emailField)

            var passwordField = UITextField(frame:CGRectMake(screenWidth/2-145, screenHeight/2-18, 281.4, 43.4))
            passwordField.backgroundColor = UIColor.whiteColor()
            passwordField.borderStyle = UITextBorderStyle.Line
            passwordField.placeholder = "Password"
            passwordField.borderStyle = .RoundedRect
            self.view.addSubview(passwordField)


        }else if screenHeight == 568 {
            
           
        }else if screenHeight == 736 {
            
            
        
    }
    }
    
    func backPressed(sender: UIButton!){
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func loginvfbPressed(sender : UIButton!){
        println("LOGIN VIA FACEBOOK")
    }
    
    func loginPressed(sender: UIButton!){
       // println("LoginPressed")
        let testView = self.storyboard?.instantiateViewControllerWithIdentifier("testView") as CustomerLoginViewController
        self.navigationController?.pushViewController(testView, animated: true)

    }
    func forgotPressed(sender: UIButton!){
        
        let forgotView = self.storyboard?.instantiateViewControllerWithIdentifier("forgotView") as ForgotPassword
        self.navigationController?.pushViewController(forgotView, animated: true)
       

    }
   
}
